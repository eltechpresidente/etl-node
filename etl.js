const fs = require("fs");
const mysql = require("mysql2");

require("dotenv").config();

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

function calculateTax(salary) {
  return salary * 0.1;
}

function checkForDuplicates(identifier) {
  return new Promise((resolve, reject) => {
    connection.query(
      "SELECT COUNT(*) as count FROM employee_table WHERE id = ?",
      [identifier],
      (err, results) => {
        if (err) {
          reject(err);
          return;
        }
        resolve(results[0].count > 0);
      }
    );
  });
}

fs.readFile("sample_employees.json", "utf-8", async (err, data) => {
  if (err) {
    console.error("Error reading file: ", err);
    return;
  }

  const employees = JSON.parse(data);

  // transform
  const transformedEmployees = employees.map((employee) => ({
    id: employee.id,
    name: employee.name,
    department: employee.department,
    salary: employee.salary,
    tax: calculateTax(employee.salary),
  }));

  // Load
  // update this code to use a for...of loop for asynchronous database query
  for (const employee of transformedEmployees) {
    const { id, ...dataWithOutId } = employee;

    try {
      const isDuplicate = await checkForDuplicates(id);
      if (!isDuplicate) {
        connection.query(
          "INSERT INTO employee_table SET ?",
          dataWithOutId,
          (err, results) => {
            if (err) {
              console.error("Error insterting data: ", err);
              return;
            }
            console.log("Inserted employee with ID:", results.insertId);
          }
        );
      } else {
        console.log("Duplicate record found for ID: ", id);
        connection.query(
          `
          DELETE FROM employee_table
          WHERE id IN (
            SELECT id
            FROM (
              SELECT id,
              ROW_NUMBER() OVER(PARTITION BY name, department, salary, tax ORDER BY id) as rn
              FROM employee_table
            ) t
            WHERE t.rn > 1
          );
          `,
          (err, results) => {
            if (err) {
              console.log("Error removing duplicates: ", err);
              return;
            }
            console.log(
              "Removed duplicates, rows affectd: ",
              results.affectedRows
            );
          }
        );
      }
    } catch (error) {
      console.error("Duplicate checking for duplicates: ", error);
    }
  }

  connection.end();
});
